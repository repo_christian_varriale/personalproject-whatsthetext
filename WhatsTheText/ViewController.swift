//
//  ViewController.swift
//  WhatsTheText
//
//  Created by Christian Varriale on 22/03/2020.
//  Copyright © 2020 Christian Varriale. All rights reserved.
//

import UIKit
import Vision
import VisionKit
import Firebase
import NaturalLanguage

// Main view controller that invokes the VisionKit document camera, and performs a VNRecognizeTextRequest on the scanned document. After That translate the text with Firebase MLKit and finally using CoreML Recognize if the text is positive or negative

class ViewController: UIViewController {
    
    //MARK: - IBOutlet
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelTranslated: UILabel!
    @IBOutlet weak var sentimentLabel: UILabel!
    @IBOutlet weak var sentimentImage: UIImageView!
    
    //MARK: - Properties
    //Convert ML Model -> NL Model
    private lazy var sentimentClassifier: NLModel? = {
        let model = try? NLModel(mlModel: TextSentimentClassifier().model)
        return model
    }()
    
    // Vision requests to be performed on each page of the scanned document.
    private var requests = [VNRequest]()
    
    // Dispatch queue to perform Vision requests.
    private let textRecognitionWorkQueue = DispatchQueue(label: "TextRecognitionQueue", qos: .userInitiated, attributes: [], autoreleaseFrequency: .workItem)
    
    //Variable that will contain the text from the scan
    private var resultingText = ""
    
    //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupVision()
    }
    
    //MARK: - Function Translate En -> It
    func translation(text: String) {
        
        // Create an English-Italian translator:
        let options = TranslatorOptions(sourceLanguage: .en, targetLanguage: .it)
        let englishItalianTranslator = NaturalLanguage.naturalLanguage().translator(options: options)
        
        //Download Model
        let conditions = ModelDownloadConditions(
            allowsCellularAccess: false,
            allowsBackgroundDownloading: true
        )
        
        englishItalianTranslator.downloadModelIfNeeded(with: conditions) { error in
            guard error == nil else { return }
            
            // Model downloaded successfully. Okay to start translating.
            englishItalianTranslator.translate(text) { translatedText, error in
                guard error == nil, let translatedText = translatedText else { return }
                
                // Translation succeeded.
                self.labelTranslated.text = translatedText
            }
        }
    }
    
    // Setup Vision request as the request can be reused
    private func setupVision() {
        
        //Recognize all the possible text from the analysis of the image
        let textRecognitionRequest = VNRecognizeTextRequest { (request, error) in
            
            //Put in a vector all the possible character recognized observed in the image
            guard let observations = request.results as? [VNRecognizedTextObservation] else {
                print("The observations are of an unexpected type.")
                return
            }
            
            // Concatenate the recognised text from all the observations.
            let maximumCandidates = 1
            for observation in observations {
                guard let candidate = observation.topCandidates(maximumCandidates).first else { continue }
                self.resultingText += candidate.string + "\n"
            }
        }
        
        // specify the recognition level
        textRecognitionRequest.recognitionLevel = .accurate
        self.requests = [textRecognitionRequest]
    }
    
    //MARK: - IBAction
    @IBAction func scanReceipts(_ sender: UIBarButtonItem) {
        //Instatiate the documentCameraViewController, Delegate and presenting it
        let documentCameraViewController = VNDocumentCameraViewController()
        documentCameraViewController.delegate = self
        present(documentCameraViewController, animated: true)
    }
    
    @IBAction func translatePressed(_ sender: UIButton) {
        translation(text: resultingText)
    }
    
    @IBAction func showSentiment(_ sender: UIButton) {
        if let label = sentimentClassifier?.predictedLabel(for: self.textView.text) {
            switch label {
            case "Positive":
                print(label)
                self.sentimentImage.image = UIImage(named: "positive")
                self.sentimentLabel.text = "Positive"
            case "Negative":
                print(label)
                self.sentimentImage.image = UIImage(named: "negative")
                self.sentimentLabel.text = "Negative"
            default:
                print(label)
                self.sentimentImage.image = UIImage(named: "neutral")
                self.sentimentLabel.text = "Neutral"
            }
        }
    }
    
}

// MARK: VNDocumentCameraViewControllerDelegate
extension ViewController: VNDocumentCameraViewControllerDelegate {
    
    public func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        
        // Clear any existing text.
        textView?.text = ""
        
        // dismiss the document camera
        controller.dismiss(animated: true)
        
        activityIndicator.isHidden = false
        
        //In async way, for each document that i have scanned, use a request handler that perform the operations on the requests (text founded)
        textRecognitionWorkQueue.async {
            self.resultingText = ""
            for pageIndex in 0 ..< scan.pageCount {
                let image = scan.imageOfPage(at: pageIndex)
                if let cgImage = image.cgImage {
                    let requestHandler = VNImageRequestHandler(cgImage: cgImage, options: [:])
                    
                    do {
                        try requestHandler.perform(self.requests)
                    } catch {
                        print(error)
                    }
                }
                self.resultingText += "\n\n"
            }
            
            DispatchQueue.main.async(execute: {
                self.textView.text = self.resultingText
                self.activityIndicator.isHidden = true
            })
        }
        
    }
}
